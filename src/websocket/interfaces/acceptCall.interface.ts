export interface AcceptCall {
  signal: any;
  to: string;
}
