import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { CallUser } from './interfaces/callUser.interface';
import { AcceptCall } from './interfaces/acceptCall.interface';
import { CloseCall } from './interfaces/closeCall.interface';

@WebSocketGateway(4000)
export class CallGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() server: Server;

  private activeSockets: string[] = [];

  handleConnection(client: Socket): void {
    if (!this.activeSockets[client.id]) {
      this.activeSockets.push(client.id);
    }

    this.server.emit('updateUsers', {
      users: this.activeSockets,
    });
  }

  handleDisconnect(client: Socket): void {
    this.activeSockets = this.activeSockets.filter(
      (existingSocket) => existingSocket !== client.id,
    );
    this.server.emit('updateUsers', { users: this.activeSockets });
  }

  @SubscribeMessage('callUser')
  handleCallUser(client: Socket, data: CallUser): void {
    client.to(data.userToCall).emit('callOffer', {
      signal: data.signalData,
      from: client.id,
    });
  }

  @SubscribeMessage('acceptCall')
  handleMakeAnswer(client: Socket, data: AcceptCall): void {
    client.to(data.to).emit('callAccepted', data.signal);
  }

  @SubscribeMessage('rejected')
  handleClose(client: Socket, data: CloseCall): void {
    client.to(data.to).emit('rejected');
  }
}
